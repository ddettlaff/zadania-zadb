#!/usr/bin/env python
# -*- coding: utf-8 -*-
import tailer
import requests
import re
import json
from daemon import runner


log_file = '/var/log/apache2/access.log'
url = "http://194.29.175.241:5984/p8/"
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
format_pat= re.compile(
    r"(?P<host>(?:[\d\.]|[\da-fA-F:])+)\s"
    r"(?P<identity>\S*)\s"
    r"(?P<user>\S*)\s"
    r"\[(?P<time>.*?)\]\s"
    r'"(?P<request>.*?)"\s'
    r"(?P<status>\d+)\s"
    r"(?P<bytes>\S*)\s"
    r'"(?P<referer>.*?)"\s'
    r'"(?P<user_agent>.*?)"\s*'
)


class   App:

    def     __init__(self):
        self.stdin_path = '/dev/null'
        self.stdout_path = '/dev/tty'
        self.stderr_path = '/dev/tty'
        self.pidfile_path = '/home/p8/dom/daemon1.pid'
        self.pidfile_timeout = 5

    def run(self):
            for line in tailer.follow(open(log_file)):
                parsed_log = format_pat.match(line).groupdict()
                data = {'host': parsed_log['host'], 'bytes': parsed_log['bytes']}
                # print data
                requests.post(url, data=json.dumps(data), headers=headers)

if  __name__ == "__main__":
    app =  App()
    daemon_runner = runner.DaemonRunner(app)
    daemon_runner.do_action()

